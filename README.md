# README #

### What is this? ###

Some dashboards that are really useful to monitor Data Model Acceleration, especially if you are using Splunk Enterprise Security.

Check out my site to learn more about it: https://www.gabrielvasseur.com/post/running-splunk-enterprise-security-at-capacity-with-data-model-acceleration

### How do I get set up? ###

Create a new dashboard in your splunk environment, edit the source, and paste the content of the XML file provided.

You then might want to edit the "host=\*" in all the searches to narrow it down to your search heads.

### So where is it? ###

<--- Please go to "Downloads" right there
